const User = require('../models/user');
const Credentials = require('../models/credetials');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const fs = require("fs");

class userController {
    async getUser(req, res) {
        try {
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const cred = await Credentials.findById(decoded.id);
            const user = await User.find({username: cred.username});
            fs.appendFile('logs.log', `Code 200: User info received successfully.\n`, () => {
            });
            return res.status(200).json(user);
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async deleteUser(req, res) {
        try {
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const cred = await Credentials.findById(decoded.id);
            await User.find({username: cred.username}).deleteOne();
            await cred.deleteOne();
            fs.appendFile('logs.log', `Code 200: User was successfully deleted.\n`, () => {
            });
            return res.status(200).json({message: "User was successfully deleted"});
        } catch (e) {
            console.log(e);
            res.status(500).json({message: 'Server error'});
        }
    }

    async updateUser(req, res) {
        try {
            const {oldPassword, newPassword} = req.body;
            const token = req.headers.authorization.split(' ')[1];
            const decode = jwt.decode(token);
            const cred = await Credentials.findById(decode.id);
            const validPassword = bcrypt.compareSync(oldPassword, cred.password);
            if (!validPassword) {
                fs.appendFile('logs.log', `Code 400: Wrong password.\n`, () => {});
                return res.status(400).json({message: "Wrong password"});
            }
            cred.password = bcrypt.hashSync(newPassword, 7);
            await cred.save();
            fs.appendFile('logs.log', `Code 200: Password was successfully changed.\n`, () => {});
            return res.status(200).json({message: "Password was successfully changed"});
        } catch (e) {
            console.log(e)
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }
}

module.exports = new userController();
