const User = require('../models/user');
const Credentials = require('../models/credetials');
const bcrypt = require('bcryptjs');
const {validationResult} = require('express-validator');
const {secret} = require('../config/config');
const jwt = require('jsonwebtoken');
const fs = require("fs");


const generateAccessToken = (id) => {
    const payload = {id};
    return jwt.sign(payload, secret, {expiresIn: "24h"});
}

class authController {
    async registration(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                fs.appendFile('logs.log', `Code 400: Registration error.\n`, () => {
                });
                return res.status(400).json({message: "Registration error", errors});
            }
            const {username, password} = req.body;
            const candidate = await User.findOne({username});
            if (candidate) {
                fs.appendFile('logs.log', `Code 400: User with such username already exists.\n`, () => {
                });
                return res.status(400).json({message: "User with such username already exists"});
            }
            const hashPassword = bcrypt.hashSync(password, 7);
            const credentials = await new Credentials({username: `${username}`, password: `${hashPassword}`});
            const user = await new User({username: `${credentials.username}`, createdDate: `${Date.now()}`});
            await credentials.save();
            await user.save();
            fs.appendFile('logs.log', `Code 200: User successfully created.\n`, () => {
            });
            return res.status(200).json({message: "User successfully created"});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }

    async login(req, res) {
        try {
            const {username, password} = req.body;
            const credentials = await Credentials.findOne({username});
            if (!credentials) {
                fs.appendFile('log.log', `User ${username} was not found\n`, () => {
                });
                return res.status(400).json({message: `User ${username} not found`});
            }
            const ValidPassword = bcrypt.compareSync(password, credentials.password);
            if (!ValidPassword) {
                fs.appendFile('logs.log', `Code 400: Wrong password.\n`, () => {
                });
                return res.status(400).json({message: "Wrong password"});
            }
            const token = generateAccessToken(credentials._id);
            fs.appendFile('logs.log', `Code 200: Login successful.\n`, () => {
            });
            return res.status(200).json({message: 'success', jwt_token: token});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {
            });
            res.status(500).json({message: 'Server error'});
        }
    }
}

module.exports = new authController();
