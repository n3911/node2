const {Schema, model} = require ('mongoose');

const Note = new Schema({
    userId: {type: String, unique: false, require: true},
    completed:  {type: Boolean, unique: false, require: true},
    text: {type: String, unique: false, require: false},
    createdDate: {type: Date, unique: false, require: true}
});

module.exports = model("Note", Note);
